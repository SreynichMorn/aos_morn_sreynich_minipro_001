package com.kshrd.myminiproject.helper;

import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class PaginationHelper implements RecyclerView.OnScrollChangeListener {

    public final static long PAGE_NUM=1;
    public final static long PAGE_LIMIT=15;

    private LinearLayoutManager layoutManager;
    public PaginationHelper(LinearLayoutManager layoutManager){
        this.layoutManager=layoutManager;
    }

    public PaginationHelper() {

    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        int itemCount=layoutManager.getItemCount();
        int visibleItemCount=layoutManager.getChildCount();
        int firstVisibleItemCount=layoutManager.findFirstVisibleItemPosition();

        if((visibleItemCount+firstVisibleItemCount)>=itemCount && firstVisibleItemCount>=0 && itemCount>=PAGE_LIMIT){
            //load more data

            loadMoreData();

        }

    }

    public abstract void loadMoreData() ;

}
