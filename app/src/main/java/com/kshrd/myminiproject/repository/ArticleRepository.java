package com.kshrd.myminiproject.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.kshrd.myminiproject.model.Article;
import com.kshrd.myminiproject.model.ListArticleResponse;
import com.kshrd.myminiproject.model.Pagination;
import com.kshrd.myminiproject.model.SingleArticleResponse;
import com.kshrd.myminiproject.remote.ApiClient;
import com.kshrd.myminiproject.remote.ApiService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {
    private ApiService apiService;
    public ArticleRepository(){
        apiService= ApiClient.createService(ApiService.class);
    }

    public MutableLiveData<ListArticleResponse>getListArticleByPagination(Pagination pagination){
        final MutableLiveData<ListArticleResponse>liveData=new MutableLiveData<>();

        //implement retrofit
        Call<ListArticleResponse> call= apiService.getAllArticle(pagination.getPage(),pagination.getLimit());
        call.enqueue(new Callback<ListArticleResponse>() {
            @Override
            public void onResponse(Call<ListArticleResponse> call, Response<ListArticleResponse> response) {
                if (response.isSuccessful()){
                    liveData.setValue(response.body());

                    Log.d("TAG Success", "onResponse: "+response.body());
                }
            }

            @Override
            public void onFailure(Call<ListArticleResponse> call, Throwable t) {
                Log.d("TAG Success", "onResponse: "+t.getMessage());
            }
        });

        return liveData;
    }

    //add article

    public MutableLiveData<SingleArticleResponse>insertArticle(Article article){
        final MutableLiveData<SingleArticleResponse>liveData=new MutableLiveData<>();

        apiService.saveArticle(article).enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
                if (response.isSuccessful()){
//                    Log.d( "tag success: ",response.body().getMessage());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
//                Log.d( "tag error: ",t.getMessage());
            }
        });

        return liveData;
    }
    //Delete article by id

    public MutableLiveData<SingleArticleResponse>deleteArticleById(int id){
        final MutableLiveData<SingleArticleResponse>liveData=new MutableLiveData<>();

        apiService.deleteData(id).enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
//                Log.d("tag delete success",response.body().getMessage());
                liveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
//                Log.d("tag delete fail",t.getMessage());
            }
        });

        return liveData;
    }


}
