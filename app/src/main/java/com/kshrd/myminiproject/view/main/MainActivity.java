package com.kshrd.myminiproject.view.main;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.kshrd.myminiproject.R;
import com.kshrd.myminiproject.model.ListArticleResponse;
import com.kshrd.myminiproject.model.SingleArticleResponse;
import com.kshrd.myminiproject.remote.RecyclerViewCallback;
import com.kshrd.myminiproject.view.adapter.ArticleAdapter;
import com.kshrd.myminiproject.view.mainViewModel.AddArticleViewModel;
import com.kshrd.myminiproject.view.mainViewModel.ListArticleViewModel;

public class MainActivity extends AppCompatActivity implements RecyclerViewCallback {

    private SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    Context context;
    SingleArticleResponse dataSet;
    ListArticleViewModel viewModel;
    ListArticleResponse articleListResponse;
    ArticleAdapter articleAdapter;
    LinearLayoutManager layoutManager;
    int currentPage=1;
    FloatingActionButton btnAdd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView=findViewById(R.id.recyclerView);

        swipeRefreshLayout=findViewById(R.id.swipeRefresh);
        setupSwipeRefresh();
        viewModel= ViewModelProviders.of(this).get(ListArticleViewModel.class);
        viewModel.init();
        getLiveDataFromViewModel();

        btnAdd=findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, AddArticleActivity.class);
                startActivity(intent);
            }
        });
    }
    private void setupData(){
        if(articleAdapter==null){
            articleAdapter=new ArticleAdapter(this,articleListResponse.getArticleList());
            layoutManager=new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(articleAdapter);
        }
        else {
            articleAdapter.setDataSet(articleListResponse.getArticleList());
            articleAdapter.notifyDataSetChanged();
        }

//        recyclerView.addOnScrollListener(new PaginationHelper(layoutManager){
//            @Override
//            public void loadMoreData() {
//
//            }
//        });
    }

    private void setupSwipeRefresh(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d("TAG refresh", "onRefresh: ");
//                viewModel.getLiveData();
//                swipeRefreshLayout.setRefreshing(false);
                viewModel.getPagination().setPage(2);
                getLiveDataFromViewModel();
            }
        });


    }
    private void getLiveDataFromViewModel(){
        viewModel.getLiveData().observe(this, new Observer<ListArticleResponse>() {
            @Override
            public void onChanged(ListArticleResponse listArticleResponse) {
                articleListResponse=listArticleResponse;
                setupData();
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getLiveDataFromViewModel();
        setupSwipeRefresh();
//        setupData();
    }

    @Override
    public void onItemClick(int position) {

        Log.d("onClick item","Item is clicked");
        Intent intent=new Intent(MainActivity.this, ArticleViewActivity.class);
        startActivity(intent);
    }

    @Override
    public void onUpdateClick(int position) {
        Log.d("onClick item","Item is Update");
    }

    @Override
    public void onDeleteClick(final int position) {

        Log.d("onClick item","Item is Delete");
        AlertDialog.Builder builder=new AlertDialog.Builder(this)
                .setTitle("Delete")
                .setMessage("Are you sure you want to delete?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        int id=articleListResponse.getArticleList().get(position).getId();
                        viewModel.deleteArticleById(id);
                        articleAdapter.notifyItemRemoved(position);
                        articleAdapter.notifyDataSetChanged();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.create();
        builder.show();
//        setupSwipeRefresh();
    }


}