package com.kshrd.myminiproject.view.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kshrd.myminiproject.R;
import com.kshrd.myminiproject.view.mainViewModel.ListArticleViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ArticleViewActivity extends AppCompatActivity {
    ImageView imageVi;
    TextView txtTitle, txtDes, txtDate;
//    MainActivityViewModel activityViewModelDetail;
    ListArticleViewModel listArticleViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_view);

        imageVi = findViewById(R.id.image_detail);
        txtTitle = findViewById(R.id.title_detail);
        txtDes = findViewById(R.id.description_detail);
        txtDate = findViewById(R.id.date_detail);

        listArticleViewModel = ViewModelProviders.of(this).get(ListArticleViewModel.class);

        Intent intent = getIntent();
        int id = intent.getIntExtra("id",0);
        listArticleViewModel.getViewById(id);


        String imageUrl = intent.getStringExtra("imageDetail");
        Glide.with(this).load(imageUrl).into(imageVi);

//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMHHmmss");
//        String localeDate = intent.getStringExtra("dateDetail");
//        Date date = null;
//        try {
//            date = dateFormat.parse(localeDate);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        dateFormat = new SimpleDateFormat("yyyy/MM/dd");
//        txtDate.setText("Created Date : "+dateFormat.format(date));

        txtTitle.setText(intent.getStringExtra("titleDetail"));
        txtDes.setText(intent.getStringExtra("descriptionDetail"));
    }
}