package com.kshrd.myminiproject.view.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.kshrd.myminiproject.R;
import com.kshrd.myminiproject.model.Article;
import com.kshrd.myminiproject.view.mainViewModel.AddArticleViewModel;
import com.kshrd.myminiproject.view.mainViewModel.ListArticleViewModel;

public class AddArticleActivity extends AppCompatActivity {
    Toolbar toolbar;
    EditText editTitle,editDes;
    ImageView imageView;
    Button btnSave,btnCancel;
    AddArticleViewModel addArticleViewModel;
    private ListArticleViewModel listArticleViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_article);

        editTitle=findViewById(R.id.editTitle);
        editDes=findViewById(R.id.editDes);
        imageView=findViewById(R.id.imageView);
        btnCancel=findViewById(R.id.btnCancel);
        btnSave=findViewById(R.id.btnSave);

        addArticleViewModel= ViewModelProviders.of(this).get(AddArticleViewModel.class);
        addArticleViewModel.init();

        btnSave.setOnClickListener(new View.OnClickListener() {
            String title,des,img;
            @Override
            public void onClick(View v) {
                title=editTitle.getText().toString();
                des=editDes.getText().toString();
                Article article=new Article();
                article.setTitle(title);
                article.setDescription(des);
                article.setImage_url("http://110.74.194.124:15011/image-thumbnails/thumbnail-21b687e3-2186-46c7-838c-e6554b7c5cad.jpeg");
                addArticleViewModel.insertArticle(article);
                finish();
            }
        });

    }
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s = cursor.getString(column_index);
        cursor.close();
        return s;
    }

}