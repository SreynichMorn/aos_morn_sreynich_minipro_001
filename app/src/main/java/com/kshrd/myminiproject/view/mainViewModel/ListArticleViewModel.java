package com.kshrd.myminiproject.view.mainViewModel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.kshrd.myminiproject.model.ArticleViewResponse;
import com.kshrd.myminiproject.model.ListArticleResponse;
import com.kshrd.myminiproject.model.Pagination;
import com.kshrd.myminiproject.model.SingleArticleResponse;
import com.kshrd.myminiproject.remote.ApiClient;
import com.kshrd.myminiproject.remote.ApiService;
import com.kshrd.myminiproject.repository.ArticleRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListArticleViewModel extends ViewModel {
    private MutableLiveData<ListArticleResponse>liveData;
    private ArticleRepository articleRepository;
    private Pagination pagination;
    private MutableLiveData<ArticleViewResponse> articleViewResponseMutableLiveData = new MutableLiveData<>();
//    private MutableLiveData<SingleArticleResponse> triggerData;
    public void init(){
        articleRepository=new ArticleRepository();
        pagination=new Pagination();
        liveData=articleRepository.getListArticleByPagination(pagination);
    }

    public MutableLiveData<ListArticleResponse>getLiveData(){
        return liveData;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void fetchListArticleByPaging() {

    }

    public void deleteArticleById(int id){
        articleRepository.deleteArticleById(id);
    }


    public void getViewById(int id){
        ApiClient.createService(ApiService.class)
                .getByOne(id)
                .enqueue(new Callback<ArticleViewResponse>() {

                    @Override
                    public void onResponse(Call<ArticleViewResponse> call, Response<ArticleViewResponse> response) {
                        if(response.isSuccessful()) {
                            ArticleViewResponse articleDetailResponse = articleViewResponseMutableLiveData.getValue();
                            articleViewResponseMutableLiveData.postValue(articleDetailResponse);
                        }
                        Log.d("getByOne","data "+response.body());
                    }

                    @Override
                    public void onFailure(Call<ArticleViewResponse> call, Throwable t) {
                        Log.e("Error Get By One", t.getMessage());
                    }
                });
    }
}
