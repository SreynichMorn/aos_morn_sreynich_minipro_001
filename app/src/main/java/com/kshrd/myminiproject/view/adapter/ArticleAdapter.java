package com.kshrd.myminiproject.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kshrd.myminiproject.R;
import com.kshrd.myminiproject.model.Article;
import com.kshrd.myminiproject.model.ListArticleResponse;
import com.kshrd.myminiproject.remote.RecyclerViewCallback;

import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder> {

    Context context;
//    ListArticleResponse articleList;
    List<Article> dataSet;
    private RecyclerViewCallback callback;
    public ArticleAdapter(Context context, List<Article> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
        try {
            this.callback= (RecyclerViewCallback) context;
        }catch (ClassCastException e){
            e.printStackTrace();
        }
    }
    public void setDataSet(List<Article> dataSet){
        this.dataSet=dataSet;
    }

    @NonNull
    @Override
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ArticleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.show_items,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleViewHolder holder, int position) {
        holder.title.setText(dataSet.get(position).getTitle());
        Glide.with(context)
                .load(dataSet.get(position).getImage_url())
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        if(dataSet==null){
            return 0;
        }
        else {
            return dataSet.size();
        }
    }

    public class ArticleViewHolder extends RecyclerView.ViewHolder {
        TextView title,description;
        ImageView imageView;
        ProgressBar progressBar;
        Button btnEdit,btnDelete;
        public ArticleViewHolder(@NonNull View itemView) {
            super(itemView);

            title=itemView.findViewById(R.id.txtTitle);
            description=itemView.findViewById(R.id.txtDes);
            imageView=itemView.findViewById(R.id.imageView);
//            progressBar=itemView.findViewById(R.id.progress_load);
            btnDelete=itemView.findViewById(R.id.btnDelete);
            btnEdit=itemView.findViewById(R.id.btnEdit);

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onDeleteClick(getAdapterPosition());
                }
            });

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onUpdateClick(getAdapterPosition());
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(getAdapterPosition());
                }
            });
        }
    }
}
