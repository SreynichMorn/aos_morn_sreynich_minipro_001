package com.kshrd.myminiproject.view.mainViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kshrd.myminiproject.model.Article;
import com.kshrd.myminiproject.model.SingleArticleResponse;
import com.kshrd.myminiproject.repository.ArticleRepository;

public class AddArticleViewModel extends ViewModel {
    private MutableLiveData<SingleArticleResponse> triggerData;
    private ArticleRepository articleRepository;
    public void init(){
        articleRepository=new ArticleRepository();
        triggerData=new MutableLiveData<>();
    }
    public void insertArticle(Article article){
        triggerData=articleRepository.insertArticle(article);
    }
    public MutableLiveData<SingleArticleResponse>getTriggerData(){
        return triggerData;
    }
}
