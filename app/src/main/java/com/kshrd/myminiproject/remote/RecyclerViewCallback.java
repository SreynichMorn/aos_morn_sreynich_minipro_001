package com.kshrd.myminiproject.remote;

public interface RecyclerViewCallback {
    void onItemClick(int position);
    void onUpdateClick(int position);
    void onDeleteClick(int position);
}
