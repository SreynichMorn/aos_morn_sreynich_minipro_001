package com.kshrd.myminiproject.remote;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static final String BASE_URL = "http://110.74.194.124:15011/";
    private static final String API_KEY="Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=";

    private static OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();


    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    public static <S> S createService(Class<S> serviceClass){
        okHttpBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request= chain.request();
                Request.Builder reqBuilder = request.newBuilder()
                        .addHeader("Authorization",API_KEY)
                        .header("Accept","application/json")
                        .method(request.method(),request.body());
                return chain.proceed(reqBuilder.build());
            }

        });
        return builder
                .client(okHttpBuilder.build())
                .build().create(serviceClass);
    }
}
