package com.kshrd.myminiproject.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListArticleResponse {
    @SerializedName("code")
    String code;
    @SerializedName("message")
    String message;
    @SerializedName("pagination")
    private Pagination pagination;
    @SerializedName("data")
    List<Article> articleList;

    public ListArticleResponse(String code, String message, Pagination pagination, List<Article> articleList) {
        this.code = code;
        this.message = message;
        this.pagination = pagination;
        this.articleList = articleList;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public List<Article> getArticleList() {
        return articleList;
    }

    public void setArticleList(List<Article> articleList) {
        this.articleList = articleList;
    }

    @Override
    public String toString() {
        return "ListArticleResponse{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", pagination=" + pagination +
                ", articleList=" + articleList +
                '}';
    }
}
