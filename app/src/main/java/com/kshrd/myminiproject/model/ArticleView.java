package com.kshrd.myminiproject.model;

import com.google.gson.annotations.SerializedName;

public class ArticleView {
    private int id;
    private String title;
    private String description;
    @SerializedName("image")
    private String imageUrl;
    @SerializedName("created_date")
    private String date;

    public ArticleView(int id, String title, String description, String imageUrl, String date) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
