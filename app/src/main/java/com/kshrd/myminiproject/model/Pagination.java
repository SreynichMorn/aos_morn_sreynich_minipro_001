package com.kshrd.myminiproject.model;

import com.google.gson.annotations.SerializedName;

public class Pagination {
    @SerializedName("limit")
     private int limit;
    @SerializedName("total_count")
     private int total_count;
    @SerializedName("total_page")
     private int total_pages;
    @SerializedName("page")
    private int page;


    public Pagination(){
        this.page=1;
        this.limit=15;
    }
    public Pagination(int limit, int total_count, int total_pages, int page) {
        this.limit = limit;
        this.total_count = total_count;
        this.total_pages = total_pages;
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
